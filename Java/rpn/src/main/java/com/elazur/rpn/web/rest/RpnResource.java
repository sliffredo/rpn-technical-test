package com.elazur.rpn.web.rest;

import com.elazur.rpn.service.RpnService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rpn")
public class RpnResource {

    private final RpnService rpnService;
    public RpnResource(RpnService rpnService) {
        this.rpnService = rpnService;
    }

    @PostMapping("/tranform-to-rpn") 
    public String transformToRpn(@RequestBody String expressionToTransform) {
        return rpnService.transformToRpn(expressionToTransform);

    }
}